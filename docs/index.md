[![Github](https://img.shields.io/badge/sources-github-green.svg)](https://github.com/mredenti/Kokkos/)

# Kokkos 

This repository contains the exercises for the School on GPU computing to be hosted by Cineca on ...data.... 

Following is a list of exercises and their description to workon during the lecture. 

| Name                                             | Description   | Level |
|--------------------------------------------------|---------------|-------|
| [Tutorial 00: Installing Kokkos](./tutorials/installation/index.md) | Kokkos build system | beginner |
| [Tutorial 01: A basic Kokkos program](./tutorials/vectorAdd/index.md) | Introduction to heterogeneous parallel programming | beginner |
| [Tutorial 02: Vector Addition in Kokkos](./tutorials/MatMul/index.md)   | Introduction to Kokkos parallel patterns | beginner | 
| [Tutorial 03: Path Tracing](./tutorials/MatMul/index.md)   | Applications | fun | 

## Authors

* **Michael Redenti** - *Initial work* 

See also the list of [contributors](https://github.com/mredenti/Kokkos/graphs/contributors) who participated in this project.

## Issues / Feature request

You can submit bug / issues / feature request using [Tracker](https://github.com/mredenti/Kokkos/issues).

## License

TBD
